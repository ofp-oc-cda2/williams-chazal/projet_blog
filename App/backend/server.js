const express = require('express');
const bodyParser = require('body-parser')
const userRoutes = require('./routes/user.routes')
require('dotenv').config({path:'./config/.env'})
const app = express();

require('./config/dbConfig');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


//routes
app.use('/users', userRoutes);





//serveur
app.listen(process.env.PORT, () => {
  console.log(`Server connected to port ${process.env.PORT}`);
});
app.get("/", (req, res) => {
    res.json({ message: "Bonjour Williams." });
  });
