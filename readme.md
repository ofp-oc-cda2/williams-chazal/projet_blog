# Conception du projet

## Projet de Blog

## Déscription du projet

Création d'un CRUD  blog avec upload d'image et aussi pour apprendre la création d'utilisateur en utilisant node et JWT. 

## Technologie utilisées :

    * REACT 
    * Node 
    * MongoDB
    * Express


## Maquette :

<img src="maquetteBlog.png"
     alt="Maquette du projet"
     style="center" />
    
